# Variable 
variable "ec2_count" {
  description = "number of count provisioning for ec2"
  default     = "1"
}

variable "ami_id" {
  description = "AMI for provisioning for ec2"
  default = "ami-0be2609ba883822ec"
}

variable "instance_type" {
  description = "type of ec2 for provisioning"
  default     = "t2.micro"
}


variable "ec2_name" {
  description = "tag name of ec2"
  default = "test_gitlab_1"
}

variable "creator_id" {
 default = "Lalit_mohan"
}

variable "region_id" {
 default = "us-east-1"
}

variable "auto_id" {
 default = "opt-in"
}
variable "account_id" {
default = "779614326491"
}
variable "key_pair" {
default = "2Test_key"
}
